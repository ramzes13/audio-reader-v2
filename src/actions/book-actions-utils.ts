import { BookMetaTextDataInterface } from '../reducers/index.t';

const AVLTree = require('binary-search-tree').AVLTree

export {
  buildBookMetaUtil,
  getActiveRegionByTime,
  getTrainingDataByStartAndLength,
}

export interface TrainingDataByStartAndLength {
  text: string,
  start: number,
  end: number,
}

function getTrainingDataByStartAndLength(id: string, textLength: number, regions: Array<BookMetaTextDataInterface>): TrainingDataByStartAndLength {
  let text = '';
  let startFound = false;
  let start = 0;
  let end = 0;
  for (let i = 0; i < regions.length; i++) {
    const region = regions[i];
    // console.log({ region })
    if (!startFound && region.id === id) {
      startFound = true;
      start = region.start;
    }

    if (startFound) {
      if (text.length < textLength) {
        text += region.text
      } else {
        end = region.end;
        break;
      }
    }
  }
  // console.log({ text, start, end })
  return { text, start, end };
}

function compareKeys(a: BookMetaTextDataInterface, b: BookMetaTextDataInterface) {
  if (a.start <= b.start && a.end <= b.end) { return -1; }
  if (a.start > b.start && a.end > b.end) { return 1; }
  return 0;
}

function buildBookMetaUtil(regions: Array<BookMetaTextDataInterface>) {
  const tree = new AVLTree({ compareKeys });
  const treePrefix = new AVLTree();
  regions.forEach(el => {
    const key = generateCfiPrefixKey(el.cfi);
    treePrefix.insert(key, el);
    tree.insert({ start: Number(el.start), end: Number(el.end) }, el);
  });
  return {
    getRegionByTime: (time: number) => {
      return getActiveRegionByTime(time, tree);
    },
    getRegionByCfi: (cfi: string) => {
      const key = generateCfiPrefixKey(cfi);
      return treePrefix.search(key).pop();
    }
  }
}

function generateCfiPrefixKey(cfi: string): string {
  const cfiRangeMeta = cfi.split(',');
  cfiRangeMeta.pop(); // remove cfi end range
  return cfiRangeMeta.join('');
}

function getActiveRegionByTime(time: number, tree: any): BookMetaTextDataInterface | undefined {
  return tree.search({ start: time, end: time }).pop();
}
