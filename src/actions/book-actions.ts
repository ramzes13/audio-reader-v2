import { AnyAction } from 'redux'
import { ThunkDispatch } from 'redux-thunk'
import { AxiosResponse } from "axios";
import debug from 'debug';

import bookMetaService from "../services/book";
import { BookMetaTextDataInterface, BookMetaInterface } from '../reducers/index.t';
import { buildBookMetaUtil } from './book-actions-utils';

const logger = debug('ar:book-actions')

export const bookActions = {
  BOOK_REQUEST: 'BOOK_REQUEST',
  BOOK_SUCCESS: 'BOOK_SUCCESS',
  BOOK_FAILURE: 'BOOK_FAILURE',
  BOOK_ACTIVE_REGION_CHANGED: 'BOOK_ACTIVE_REGION_CHANGED',
  BOOK_TEXT_SELECTED: 'BOOK_TEXT_SELECTED',
};

let bookMetaUtil: any;

const loadBookMeta = (id: number) => {
  return ((dispatch: ThunkDispatch<{}, {}, AnyAction>) => {

    dispatch({ type: bookActions.BOOK_REQUEST });
    bookMetaService.get(id)
      .then(({ data }: AxiosResponse) => {
        const regions: Array<BookMetaTextDataInterface> = data.textData;
        bookMetaUtil = buildBookMetaUtil(regions);

        dispatch({ type: bookActions.BOOK_SUCCESS, payload: data });
      })
      .catch(error => {
        dispatch({ type: bookActions.BOOK_FAILURE, payload: error, error: true });
      });
  });
}

const audioTimeUpdate = (time: Number) => {
  return ((dispatch: ThunkDispatch<{}, {}, AnyAction>, getState: any) => {
    const activeRegion = bookMetaUtil.getRegionByTime(time);
    if (activeRegion) {
      const state: BookMetaInterface = getState();

      if (!state.activeRegion || state.activeRegion.id !== activeRegion.id) {
        dispatch({ type: bookActions.BOOK_ACTIVE_REGION_CHANGED, activeRegion })
      }
    }
  });
}

function textSelected(cfi: string, selectedTex: string) {
  return ((dispatch: ThunkDispatch<{}, {}, AnyAction>) => {
    const activeRegion: BookMetaTextDataInterface = bookMetaUtil.getRegionByCfi(cfi);
    logger({ activeRegion, cfi, selectedTex })
    if (activeRegion) {
      const startTime = Number(activeRegion.start);
      const selectedTextLength = selectedTex.length;

      dispatch({ type: bookActions.BOOK_TEXT_SELECTED, startTime, selectedTextLength })
    }
  });
}

export {
  loadBookMeta,
  textSelected,
  audioTimeUpdate,
}