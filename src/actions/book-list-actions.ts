import { AnyAction } from 'redux'
import { ThunkDispatch } from 'redux-thunk'
import { AxiosResponse } from "axios";

import bookListService from "../services/book-list";

export const bookListActions = {
  BOOK_LIST_REQUEST: 'BOOK_LIST_REQUEST',
  BOOK_LIST_SUCCESS: 'BOOK_LIST_SUCCESS',
  BOOK_LIST_FAILURE: 'BOOK_LIST_FAILURE',
};

const loadBookList = () => {
  return ((dispatch: ThunkDispatch<{}, {}, AnyAction>) => {
    dispatch({ type: bookListActions.BOOK_LIST_REQUEST });
    bookListService.find()
      .then((response: AxiosResponse) => {
        dispatch({ type: bookListActions.BOOK_LIST_SUCCESS, payload: response.data });
      })
      .catch(error => {
        dispatch({ type: bookListActions.BOOK_LIST_FAILURE, payload: error, error: true });
      });
  });
}

export {
  loadBookList,
}