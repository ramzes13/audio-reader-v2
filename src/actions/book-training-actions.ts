import { AnyAction } from 'redux'
import { ThunkDispatch } from 'redux-thunk'

import { getTrainingDataByStartAndLength } from './book-actions-utils';
import { AppState } from '../reducers/index.t';
export const bookTrainingActions = {
  BOOK_TRAINING_START: 'BOOK_TRAINING_START',
  BOOK_TRAINING_STOP: 'BOOK_TRAINING_STOP',
};

function actionBookTrainingStartTraining() {
  //todo add selected text and audio range
  return ((dispatch: ThunkDispatch<{}, {}, AnyAction>, getState: any) => {
    const { book, bookTraining }: AppState = getState();
    const { activeRegion, meta } = book;
    const { textLength } = bookTraining;

    if (!activeRegion) {
      return;
    }

    const { text, start, end } = getTrainingDataByStartAndLength(activeRegion.id, textLength, meta.textData);
    const audio = { start, end };
    dispatch({ type: bookTrainingActions.BOOK_TRAINING_START, text, audio })
  });
}

function actionBookTrainingStopTraining() {
  return { type: bookTrainingActions.BOOK_TRAINING_STOP }
}

export {
  actionBookTrainingStartTraining,
  actionBookTrainingStopTraining
}