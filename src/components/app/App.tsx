import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import { BookMetaInterface, BookTrainingInterface } from '../../reducers/index.t';
import { createLoadingSelector } from '../../reducers/api/selectors';
import { loadBookMeta } from '../../actions/book-actions';
import AudioHtml from './audio-html/audio-html';
import TextEpub from './text-epub/text-epub';

import LoadingIndicator from '../../ui/loader-indicator';

import './App.scss';

type PathParamsType = {
  id: string
}

interface DispatchProps {
  loadBookMeta: (id: number) => void;
}

interface AppComponentPropsInterface {
  book: BookMetaInterface,
  bookTraining: BookTrainingInterface,
  isFetching: boolean
}

type PropsType = RouteComponentProps<PathParamsType> & AppComponentPropsInterface & DispatchProps;

class App extends Component<PropsType>{
  componentDidMount() {
    this.props.loadBookMeta(parseInt(this.props.match.params.id));
  }

  render() {
    const { isFetching } = this.props;
    const { loaded, meta } = this.props.book;
    const { trainingMode } = this.props.bookTraining;
    const content = [];

    if (!isFetching && loaded) {
      // content.push(<AudioHtml audioUrl={meta.audio.audioUri} key="audio-html" />);
      // if (trainingMode) {
      //   content.push(<BookTraining key="text-training" />);
      // } else {

      content.push(<TextEpub key="text-epub" />);
      // }
    }

    return (
      <div className="App">
        {content}
        {/* <AudioHtml key="audio-html" /> */}
        {/* {isFetching && <LoadingIndicator />}
        {content} */}
      </div>
    );
  }
}

const loadingSelector = createLoadingSelector(['BOOK']);

const mapStateToProps = (state: any): AppComponentPropsInterface => ({ book: state.book, bookTraining: state.bookTraining, isFetching: loadingSelector(state) })

const mapDispatchToProps = {
  loadBookMeta,
};

export default withRouter(
  connect<AppComponentPropsInterface, DispatchProps>(
    mapStateToProps,
    mapDispatchToProps
  )(App))