import React, { Component, createRef, MouseEvent } from 'react';
import debug from 'debug';
import './audio-html.scss'

const logger = debug('ar:audio-html')

type Props = {
  audioUrl: string,
  start: number,
  end: number,
  onTimeUpdate: (id: number) => void,
};
type State = {
  loaded: boolean,
  playing: boolean,
  totalTime: number,
  currentTime: number,
  progressPercent: number,
}

class AudioHtml extends Component<Props, State> {
  player = createRef<HTMLAudioElement>();
  seekSlider: HTMLInputElement | null;
  draggableClasses: Array<string>;

  public static defaultProps = {
    start: 0,
    end: 0
  };

  constructor(props: Props) {
    super(props);

    this.togglePlay = this.togglePlay.bind(this);
    this.rewind = this.rewind.bind(this);
    this.seekSlider = null;
    this.draggableClasses = ['pin'];

    this.state = {
      loaded: false,
      playing: false,
      totalTime: 0,
      currentTime: 0,
      progressPercent: 0
    }

    logger('constructor', this.props)
  }

  togglePlay() {
    const player = this.player.current!;
    const { playing } = this.state;

    playing ? player.pause() : player.play();
    logger('togglePlay', { playing })
    this.setState({
      playing: !playing
    });
  }

  componentDidMount() {
    logger('componentDidMount');
    // this.seekSlider && this.seekSlider.addEventListener("nv-enter", this.handleSeekSlider);
    const player = this.player.current!;

    player.addEventListener('timeupdate', this.updateProgress.bind(this));
    player.addEventListener('volumechange', this.updateVolume.bind(this));

    player.addEventListener('ended', () => {
      logger('ended event')
      this.setState({ playing: false });
    });

    player.addEventListener('loadedmetadata', () => {
      const start = this.props.start;
      const end = this.props.end || player.duration;
      logger({ start, end });

      player.currentTime = start
      const totalTime = end - start;
      this.setState({ loaded: true, totalTime })
    });

  }

  inRange(event: MouseEvent) {
    const rangeBox = this.getRangeBox(event);
    const sliderPositionAndDimensions = rangeBox.getBoundingClientRect() as DOMRect;
    const clientX = event.clientX;
    const min = sliderPositionAndDimensions.x;
    const max = min + sliderPositionAndDimensions.width;

    if (clientX < min || clientX > max) return false;

    return true;
  }

  isDraggable(el: Element) {
    let canDrag = false;

    if (typeof el.classList === 'undefined') return false; // fix for IE 11 not supporting classList on SVG elements

    for (let i = 0; i < this.draggableClasses.length; i++) {
      if (el.classList.contains(this.draggableClasses[i])) {
        canDrag = true;
      }
    }

    return canDrag;
  }

  updateProgress() {
    if (!this.player.current) {
      logger('updateProgress player is not defined')
      return;
    }

    const player = this.player.current;
    const { totalTime } = this.state;
    const { start, end } = this.props;
    const relativeEndTime = totalTime + start;
    const hasReachEnd = player.currentTime >= relativeEndTime

    const currentTime = hasReachEnd ? relativeEndTime : player.currentTime;
    const relativeCurrentTime = currentTime - start;

    const progressPercent = (relativeCurrentTime / totalTime) * 100;

    this.props.onTimeUpdate(currentTime);
    const playing = this.state.playing && !hasReachEnd;
    logger('updateProgress', player.currentTime, { playing, hasReachEnd, progressPercent })

    // set current time only if player is playing, to fix infinite loop, update progress and reset time
    if (hasReachEnd && this.state.playing) {
      logger('updateProgress set player current time', this.state.totalTime)
      player.pause();
      player.currentTime = this.state.totalTime;
    }

    this.setState({ progressPercent, currentTime, playing });
  }

  rewind(event: MouseEvent) {
    logger('rewind')
    if (this.inRange(event)) {
      const player = this.player.current!;
      player.currentTime = this.state.totalTime * this.getCoefficient(event);
    }
  }

  getCoefficient(event: MouseEvent) {
    const slider = this.getRangeBox(event);
    const sliderPositionAndDimensions = slider.getBoundingClientRect();
    const clientX = event.clientX;
    const offsetX = clientX - sliderPositionAndDimensions.left;
    const { width } = sliderPositionAndDimensions;
    return offsetX / width;
  }

  getRangeBox(event: MouseEvent): HTMLElement {
    let rangeBox = event.target! as HTMLElement;
    // const el = this.currentlyDragged;
    // if (event.type === 'click' && this.isDraggable(event.target)) {
    //   rangeBox = event.target.parentElement.parentElement;
    // }
    // if (event.type === 'mousemove') {
    //   rangeBox = el.parentElement.parentElement;
    // }
    // if (event.type === 'touchmove') {
    //   rangeBox = el.target.parentElement.parentElement;
    // }
    return rangeBox;
  }

  updateVolume() {
    console.log('update volume')
  }

  componentWillUnmount() {

  }

  handleSeekSlider() {
  }

  render() {
    const buttons = [];
    const { loaded, playing, currentTime, totalTime, progressPercent } = this.state;
    const sliderStyle = {
      width: `${progressPercent.toFixed(0)}%`
    };

    logger('audio html render', { loaded });

    if (loaded) {
      const dValue = playing ? 'M0 0h6v24H0zM12 0h6v24h-6z' : 'M18 12L0 24V0'
      buttons.push(
        <div className="play-pause-btn" key='play-pause' onClick={this.togglePlay} >
          <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
            <path fill="#566574" fillRule="evenodd" d={dValue} className="play-pause-icon" id="playPause"></path>
          </svg>
        </div>
      );
    } else {
      buttons.push(
        <div className="loading" key='loading'>
          <div className="spinner"></div>
        </div>
      )
    }

    return (
      <div className="audio-html">
        <audio ref={this.player} src={this.props.audioUrl} />
        <div className="audio green-audio-player">
          {buttons}
          <div className="controls">
            <span className="current-time">{currentTime.toFixed(0)}</span>
            <div className="slider" onClick={this.rewind}>
              <div className="progress" style={sliderStyle} >
                <div className="pin" ></div>
              </div>
            </div>
            <span className="total-time">{totalTime.toFixed(0)}</span>
          </div>
          <div className="volume">
            <div className="volume-btn">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="#566574" fillRule="evenodd"
                  d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z"
                  id="speaker"></path>
              </svg>
            </div>
            <div className="volume-controls hidden">
              <div className="slider" data-direction="vertical">
                <div className="progress">
                  <div className="pin" id="volume-pin" data-method="changeVolume"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default AudioHtml;

// const mapStateToProps = (state: any): BookMetaInterface => ({ ...state.book })

// const mapDispatchToProps = { audioTimeUpdate, audioStart, audioStop };

// export default connect<BookMetaInterface, DispatchProps>(
//   mapStateToProps,
//   mapDispatchToProps
// )(AudioHtml)
