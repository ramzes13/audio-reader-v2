import React, { Component } from 'react';
import { connect } from 'react-redux';
import debug from 'debug';

import { BookMetaInterface, BookTrainingInterface } from '../../../reducers/index.t';
import { audioTimeUpdate } from '../../../actions/book-actions';
import { actionBookTrainingStartTraining, actionBookTrainingStopTraining } from '../../../actions/book-training-actions';
import AudioHtml from '../audio-html/audio-html';

const logger = debug('ar:book-training')

interface ComponentInterface {
  book: BookMetaInterface,
  bookTraining: BookTrainingInterface,
}

interface DispatchProps {
  audioTimeUpdate: (time: Number) => void,
  actionBookTrainingStartTraining: () => void,
  actionBookTrainingStopTraining: () => void,
};

type Props = ComponentInterface & DispatchProps;
type State = {
  recording: boolean,
  ready: boolean
}
class BookTraining extends Component<Props, State> {
  mediaRecorder: MediaRecorder | undefined;

  constructor(props: Props) {
    super(props);
    this.playStopRecorder = this.playStopRecorder.bind(this);

    this.state = {
      recording: false,
      ready: false,
    }
  }

  componentDidUpdate() {
    logger('componentDidUpdate');
  }

  componentDidMount() {
    logger('componentDidMount');
  }

  async initRecorder() {
    // const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    // this.mediaRecorder = new MediaRecorder(stream);
    // let audioChunks: Array<Blob> = []

    // this.mediaRecorder.ondataavailable = (event: BlobEvent) => {
    //   audioChunks.push(event.data);
    //   logger('ondataavailable', audioChunks.length);
    // }

    // this.mediaRecorder.addEventListener("stop", (e) => {
    //   this.setState({
    //     recording: false,
    //   });
    //   logger('stop')
    //   const audioBlob = new Blob(audioChunks);
    //   const audioUrl = URL.createObjectURL(audioBlob);
    //   const audio = new Audio(audioUrl);
    //   audio.play();
    // });

    // this.mediaRecorder.addEventListener("start", (e) => {
    //   logger('start');
    //   this.setState({
    //     recording: true
    //   })
    //   audioChunks = [];
    // });

    // this.setState({
    //   ready: true,
    // });
  }

  playStopRecorder() {
    if (!this.mediaRecorder) {
      return;
    }

    if (this.mediaRecorder.state === 'inactive') {
      this.mediaRecorder.start();
    } else {
      this.mediaRecorder.stop();
    }
  }

  render() {
    const { recording, ready } = this.state;
    logger('render', { ready });
    const content = [];

    content.push(<button onClick={this.props.actionBookTrainingStopTraining} key="stop-training">Stop training</button>);
    const { text, audio } = this.props.bookTraining;
    content.push(<AudioHtml
      start={Number(audio.start)}
      end={Number(audio.end)}
      audioUrl={this.props.book.meta.audio.audioUri}
      onTimeUpdate={this.props.audioTimeUpdate}
      key="book-training-audio-html" />);
    content.push(<div key="text-container">{text}</div>);
    return (
      <div className="BookTraining" >
        {content}
      </div>
    )
  }
}

const mapStateToProps = (state: any): ComponentInterface => ({ book: state.book, bookTraining: state.bookTraining })

const mapDispatchToProps = { audioTimeUpdate, actionBookTrainingStartTraining, actionBookTrainingStopTraining };

export default connect<ComponentInterface, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(BookTraining)
