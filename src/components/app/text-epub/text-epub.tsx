import React, { Component } from 'react';
import ePub from 'epubjs';
import { Book, Rendition } from 'epubjs/types';
import { connect } from 'react-redux'
import debug from 'debug';

import AudioHtml from '../audio-html/audio-html';
import { actionBookTrainingStartTraining } from '../../../actions/book-training-actions';
import { audioTimeUpdate } from '../../../actions/book-actions';
import { BookMetaInterface, BookMetaTextDataInterface, BookTrainingInterface } from '../../../reducers/index.t';
import { textSelected } from '../../../actions/book-actions';
import BookTraining from '../book-training/book-training';

const logger = debug('ar:text-epub')

type DispatchProps = {
  textSelected: (cfi: string, selectedTex: string) => void,
  actionBookTrainingStartTraining: () => void,
  audioTimeUpdate: (id: number) => void,
}

interface AppComponentPropsInterface {
  book: BookMetaInterface,
  bookTraining: BookTrainingInterface,
}

type PropsType = AppComponentPropsInterface & DispatchProps;

let divStyle: any = {
  height: 'auto',
  display: 'block',
};

class TextEpub extends Component<PropsType, any> {
  book: Book | undefined;
  rendition: Rendition | undefined;
  annotationType: string = 'highlight';

  constructor(props: PropsType) {
    super(props);
    this.book = ePub(this.props.book.meta.epubUrl!);

    this.state = { epubInitiated: false };

    this.initBook = this.initBook.bind(this)
    this.prepareMeta = this.prepareMeta.bind(this)
    this.startTraining = this.startTraining.bind(this)
  }

  async componentDidUpdate() {
    logger('componentDidUpdate');
    await this.initBook();
  }

  async componentDidMount() {
    logger('componentDidMount');
    await this.initBook();
  }

  async prepareMeta(cfiRange: any) {
    if (!this.book) {
      return;
    }

    const range = await this.book.getRange(cfiRange);
    if (range) {
      const selectedTex = range.toString().trim();
      this.props.textSelected(cfiRange, selectedTex);
    }
  }

  initBook() {
    return new Promise((resolve) => {
      logger('initBook start')
      window.requestAnimationFrame(() => {
        const { trainingMode } = this.props.bookTraining;

        if (trainingMode || !this.book || this.state.epubInitiated) {
          logger('initBook cancel')
          return resolve()
        }

        //@ts-ignore
        this.rendition = this.book.renderTo("epub-reading-container",
          {
            flow: "scrolled-doc",
            width: "100%",
            height: "100%"
          });

        this.rendition.themes.default({
          '::selection': {
            'background': 'rgb(196, 196, 196)'
          },
          '.epubjs-hl': {
            'fill': 'yellow', 'fill-opacity': '0.3', 'mix-blend-mode': 'multiply'
          }
        });

        this.rendition.on("selected", (cfiRange: any, contents: any) => {
          this.prepareMeta(cfiRange);
        });
        logger('initBook finish')
        this.setState({ epubInitiated: true });
        return resolve();
      });
    })
  }

  clearAnnotations() {
    const _this = this;
    if (!_this.rendition) {
      return;
    }

    // @ts-ignore
    Object.keys(_this.rendition.annotations._annotations).forEach(key => {
      // @ts-ignore
      const annotation = _this.rendition.annotations._annotations[key];
      // @ts-ignore
      _this.rendition.annotations.remove(annotation.cfiRange, _this.annotationType);
    })
  }

  handleSelectedRegion(activeRegion: BookMetaTextDataInterface) {
    if (!this.rendition) {
      logger('handleSelectedRegion not ready')
      return;
    }
    this.clearAnnotations();
    this.rendition.display(activeRegion.cfi); //scroll to selection
    this.rendition.annotations.add(this.annotationType, activeRegion.cfi, {});
  }

  startTraining() {
    logger('startTraining')
    this.setState({ epubInitiated: false });
    this.props.actionBookTrainingStartTraining()
  }

  render() {
    logger('render')
    const { canTrain, trainingMode } = this.props.bookTraining;
    const content = [];

    if (trainingMode) {
      content.push(<BookTraining key="text-training" />);
    } else {
      this.handleSelectedRegion(this.props.book.activeRegion);
      if (canTrain) {
        content.push(<button onClick={this.startTraining} key="start-training">Start training</button>);
      }
      content.push(<AudioHtml audioUrl={this.props.book.meta.audio.audioUri} onTimeUpdate={this.props.audioTimeUpdate} key="epub-audio-html" />);
      content.push(<div style={divStyle} id="epub-reading-container" key='epub-container' ></div>);
    }

    return (
      <div>
        {content}
      </div>
    )
  }
}
const mapStateToProps = (state: any): AppComponentPropsInterface => ({ book: state.book, bookTraining: state.bookTraining })

const mapDispatchToProps = {
  textSelected,
  actionBookTrainingStartTraining,
  audioTimeUpdate,
};

export default connect<AppComponentPropsInterface, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps,
)(TextEpub)
