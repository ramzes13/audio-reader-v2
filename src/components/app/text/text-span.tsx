import React from 'react';
function TextSpan(props: { selected: boolean, text: string }) {
  return <span className={props.selected ? 'show' : ''}>{props.text} </span>;
}

export default TextSpan;
