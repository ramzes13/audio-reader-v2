import React, { Component } from 'react';
import { PeaksOptions, init, PeaksInstance } from 'peaks.js';
import { connect } from 'react-redux'

import { BookMetaInterface } from '../../../reducers/index.t';
import TextSpan from './text-span';
import './text.css';

type DispatchProps = {}

type Props = BookMetaInterface

class Text extends Component<Props, any> {
  render() {
    const { meta } = this.props;
    return (
      <div className="Text">
        {meta.textData.map((textSpan) => {
          const selected = !!(this.props.activeRegion && this.props.activeRegion.id == textSpan.id);

          return (<TextSpan selected={selected} key={textSpan.id} text={textSpan.text} />)
        })}
      </div>
    )
  }
}
const mapStateToProps = (state: any): BookMetaInterface => ({ ...state.book })

const mapDispatchToProps = {};

export default connect<BookMetaInterface, DispatchProps>(
  mapStateToProps,
)(Text)
