import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { RouteComponentProps } from "react-router";

import { BookListsInterface, BookListInterface } from '../../reducers/index.t';
import { loadBookList } from '../../actions/book-list-actions';
import { createLoadingSelector } from '../../reducers/api/selectors';
import LoadingIndicator from '../../ui/loader-indicator';

type PathParamsType = {}
interface DispatchProps {
  loadBookList: () => void;
}

type PropsType = RouteComponentProps<PathParamsType> & BookListsInterface & DispatchProps & {
  isFetching: boolean
}

class Home extends Component<PropsType> {

  componentDidMount() {
    this.props.loadBookList();
  }

  render() {
    const { isFetching, books } = this.props;

    const booksListContent = (booksList: Array<BookListInterface>) => booksList.map((el: BookListInterface) =>
      <li key={el.id}><Link to={`/books/${el.id}`}>{el.name}</Link></li>
    );

    return (
      <div className="Home">
        {!!isFetching && <LoadingIndicator />}
        <ul>
          {books && booksListContent(books)}
        </ul>
      </div>
    );
  }
}
const loadingSelector = createLoadingSelector(['BOOK_LIST']);

const mapStateToProps = (state: any): BookListsInterface => ({ ...state.bookList, isFetching: loadingSelector(state) })

const mapDispatchToProps = {
  loadBookList,
};

export default withRouter(
  connect<BookListsInterface, DispatchProps>(
    mapStateToProps,
    mapDispatchToProps
  )(Home))

