import { BookListsInterface } from './index.t';
import { bookListActions } from '../actions/book-list-actions';

const {
  BOOK_LIST_SUCCESS,
} = bookListActions;

const defaultState: BookListsInterface = {
  books: [],
};

// is desireble to have a error message per store prop
// like so { errors: action.error ? action.payload.message : null }

const configs = (state: BookListsInterface = defaultState, action: any): BookListsInterface => {
  switch (action.type) {
    case BOOK_LIST_SUCCESS:
      return {
        ...state,
        books: action.payload
      };
    default:
      return state
  }
}

export default configs;