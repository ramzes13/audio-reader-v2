import { BookMetaInterface, BookMetaTextDataInterface, BookDataInterface } from './index.t';
import { bookActions } from '../actions/book-actions';

const {
  BOOK_SUCCESS,
  BOOK_ACTIVE_REGION_CHANGED,
  BOOK_TEXT_SELECTED,
} = bookActions;

const defaultState: BookMetaInterface = {} as BookMetaInterface;

const configs = (state: BookMetaInterface = defaultState, action: any): BookMetaInterface => {
  switch (action.type) {
    case BOOK_SUCCESS: {
      const { payload }: { payload: BookDataInterface } = action;
      const activeRegion = payload.textData[0];
      return { ...state, activeRegion, meta: payload, loaded: true }
    };
    case BOOK_ACTIVE_REGION_CHANGED: {
      const activeRegion: BookMetaTextDataInterface = action.activeRegion;
      return { ...state, activeRegion, forcedTime: null }
    };
    case BOOK_TEXT_SELECTED: {
      const { startTime } = action;
      return { ...state, forcedTime: startTime };
    };
    default:
      return state
  }
}

export default configs;