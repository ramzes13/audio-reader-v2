import { BookTrainingInterface } from './index.t';
import { bookTrainingActions } from '../actions/book-training-actions';
import { bookActions } from '../actions/book-actions';

const {
  BOOK_TRAINING_START,
  BOOK_TRAINING_STOP,
} = bookTrainingActions;

const {
  BOOK_TEXT_SELECTED,
} = bookActions;

const defaultState: BookTrainingInterface = {} as BookTrainingInterface;

const configs = (state: BookTrainingInterface = defaultState, action: any): BookTrainingInterface => {
  switch (action.type) {
    case BOOK_TRAINING_STOP:
      return { ...state, trainingMode: false }
    case BOOK_TRAINING_START: {
      const { text, audio } = action;
      return { ...state, trainingMode: true, text, audio }
    };
    case BOOK_TEXT_SELECTED: {
      const { selectedTextLength: textLength } = action;
      let canTrain = false;

      if (textLength > 3) {
        canTrain = true;
      }

      return { ...state, canTrain, textLength };
    };
    default:
      return state
  }
}

export default configs;