import { WebAudioOptions, AudioOptions } from 'peaks.js';

export interface RegionInterface {
  id: string,
  text: string,
  start: number,
  end: number,
}

export type AppAudioType = AudioOptions & {
  url: string
};

export interface RegionsInterface extends Array<RegionInterface> { }

export interface BookListInterface {
  id: string,
  name: string,
}
export interface LoadingMetaInterface {
  isLoading: number,
  lastLoad?: Date | null,
}

export interface BookMetaTextDataInterface {
  id: string,
  start: number,
  end: number,
  text: string,
  cfi: string,
}
export interface BookDataInterface {
  audio: {
    metaUri: string,
    audioUri: string,
  },
  textData: Array<BookMetaTextDataInterface>,
  epubUrl?: string,
}
export interface BookMetaInterface {
  meta: BookDataInterface,
  playing: boolean,
  forcedTime?: number | null,
  activeRegion: BookMetaTextDataInterface,
  loaded: boolean,
}

export interface BookListsInterface {
  books: Array<BookListInterface>,
}

export interface AppState {
  regions: RegionsInterface,
  activeRegion: RegionInterface | null
  bookList: BookListsInterface,
  book: BookMetaInterface,
  bookTraining: BookTrainingInterface,
  api: any,
};

export interface BookTrainingInterface {
  canTrain: boolean,
  trainingMode: boolean,
  textLength: number,
  text: string
  audio: {
    start: number,
    end: number,
  }
};