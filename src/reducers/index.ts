import { combineReducers } from 'redux'

import regions from './regions'
import activeRegion from './active-region'
import bookList from './book-list-reducer'
import api from './api';
import book from './book-reducer';
import bookTraining from './book-training-reducer';

import {
  AppState
} from './index.t';

export default combineReducers<AppState>({
  regions,
  activeRegion,
  bookList,
  book,
  bookTraining,
  api,
})
