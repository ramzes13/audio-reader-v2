import axios, { AxiosInstance, AxiosPromise } from "axios";
export const bookListService = (axios: AxiosInstance) => ({
  find: (): AxiosPromise =>
    axios.get(`${process.env.REACT_APP_STATIC_FILE_HOST}/book-list.json`)
});

export default bookListService(axios);