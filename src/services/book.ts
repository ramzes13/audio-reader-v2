import axios, { AxiosInstance, AxiosPromise } from "axios";

export const bookService = (axios: AxiosInstance) => ({
  get: (id: number): AxiosPromise =>
    axios.get(`${process.env.REACT_APP_STATIC_FILE_HOST}/${id}/book-meta.json`)
});

export default bookService(axios);